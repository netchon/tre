import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreBackupComponent } from './tre-backup.component';

describe('TreBackupComponent', () => {
  let component: TreBackupComponent;
  let fixture: ComponentFixture<TreBackupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreBackupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreBackupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
