import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { TRE } from './tre';

@Component({
  selector: 'app-tre-backup',
  templateUrl: './tre-backup.component.html',
  styleUrls: ['./tre-backup.component.css']
})
export class TreBackupComponent implements OnInit {
  treForm:FormGroup;
  tre:TRE = new TRE();

  constructor(private fb:FormBuilder) { }

  ngOnInit() {

    this.treForm = this.fb.group(
      {
        name:['', [Validators.required, Validators.minLength(3)]],
        father_name:[''],
        mother_name:'',
        birthday_date:['', Validators.required],
        sexo:[1, Validators.required],
        marital_status:['', Validators.required],
        nationality_id:['', Validators.required],
        island_id:['', Validators.required],
        town_id:['', Validators.required],
        parish_id:['', Validators.required],
        address_id:['', Validators.required],
        locality_id:['', Validators.required],
        tel:[''],
        email:['', Validators.email],
        passportNumber:['', [Validators.required, Validators.pattern('^(?!^0+$)[a-zA-Z0-9]{3,20}$')]],
        issued_on:['', Validators.required],
        expiration_date:['', Validators.required],
        issuance_date:['', Validators.required],
        tre_type:[''],
        document_id:['']
      }
    );
  }

    save(){
      console.log(this.treForm);
      console.log('Saved: ' +JSON.stringify(this.treForm.value));
    }

}
