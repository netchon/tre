import { TipoPedido } from "./tipoPedido";

export class TRE{
    name:string;
    father_name:string;
    mother_name:string;
    birthday_date:Date;
    sexo:{masculino, feminino, outro};
    marital_status:{solteiro, casado, viuvo, "uniao de facto"};
    nationality_id:string;
    island_id:string;
    town_id:string;
    parish_id:string;
    address_id:string;
    locality_id:string;
    tel:string;
    email:string;
    passportNumber:string;
    issued_on:string;
    expiration_date:Date;
    issuance_date:Date;
    tre_type:TipoPedido;
    document_id:string;
}